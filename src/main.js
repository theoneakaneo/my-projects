import { createApp } from 'vue';
// import Vue2TouchEvents from 'vue2-touch-events'
// import { VueHammer } from 'vue2-hammer'

import App from './App.vue'
import './main.css'
// import VueAnimate from 'vue-animate-scroll'
// import AOS from 'aos'
// import 'aos/dist/aos.css'

const app = createApp(App)

// app.use(Vue2TouchEvents)
// app.use(VueHammer)
// app.use(VueAnimate)
// app.use(AOS)

app.mount('#app')
