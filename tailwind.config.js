const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  purge: [
    './src/**/*.html',
    './src/**/*.vue',
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: ['Inter var', ...defaultTheme.fontFamily.sans],
      },
    },
  },
  variants: {
    translate: ['responsive', 'hover', 'focus', 'motion-safe', 'motion-reduce'],
    scrollSnapType: ['responsive'],
    animation: ['responsive', 'hover', 'focus'],
  },
  plugins: [
    require('@tailwindcss/ui')({
      layout: 'sidebar',
    }),
    require('tailwindcss-scroll-snap'),
  ],
}
